This Repo hosts a working version of https://gns3.com/marketplace/appliances/open-vswitch-with-management-interface.

I used the fix from https://gns3.com/openvswitch-docker-issue to create my own gns3a file. Just download the file openvswitch-management-fixed.gns3a and import it like normal.
